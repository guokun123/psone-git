<?php
/**
 * 
 * @author LENOVO
 *
 */
class SpriteConfig
{
        private static $_mysqldb=array();
        private static $_mongodb=array();
        private static $_redisdb=array();
        private static $_memcached=array();
        private static $_uploadconfig=array();
		private static $_spritepath;
		private static $_logdir='';//日志目录
        public static function  getInstance()
        {
                self::$_mysqldb=array();
                self::$_mysqldb['PSONE']['dsn'] = 'mysql:host=58.135.86.95;port=6033;dbname=ea_sys';
                self::$_mysqldb['PSONE']['user'] = 'guokun';
                self::$_mysqldb['PSONE']['password'] = 'guokun123';
                self::$_mysqldb['PSONE']['charset'] = 'utf8';

                self::$_mysqldb['PSONE'][0]['dsn'] = 'mysql:host=58.135.86.95;port=6033;dbname=ea_sys';
                self::$_mysqldb['PSONE'][0]['user'] = 'guokun';
                self::$_mysqldb['PSONE'][0]['password'] = 'guokun123';
                self::$_mysqldb['PSONE'][0]['charset'] = 'utf8';
                self::$_mysqldb['PSONE'][0]['weight'] = 9;
                
                self::$_mongodb=array();
                self::$_mongodb['MONGO1']['host'] = '127.0.0.1:27017';
                self::$_mongodb['MONGO1']['dbname'] = 'xxxxxxxx';
                
                self::$_mongodb['MONGO2']['host'] = '127.0.0.1:27017';
                self::$_mongodb['MONGO2']['dbname'] = 'xxxxxxxx';
                
                self::$_redisdb=array();
                self::$_redisdb['REDISMAIN']['host'] = 'xxxxxx';
                self::$_redisdb['REDISMAIN']['port'] = 6379;
                self::$_redisdb['REDISMAIN']['password'] = 'xxxxx';
                
                self::$_redisdb['REDIS2']['host'] = 'xxxxxxxx';
                self::$_redisdb['REDIS2']['port'] = 6379;
                self::$_redisdb['REDIS2']['password'] = 'xxxxxxxx';
                
                
                self::$_memcached=array();
                self::$_memcached['MEMMAIN']=array(array('127.0.0.1', '11211'));
                self::$_memcached['MEM2']=array(array('127.0.0.1', '11211'),array('127.0.0.1', '11211'));
                
                
               self::$_uploadconfig= array(
                 		"viewUrl"=>'/upload',
    					"savePath" => "/home/zhaojin/jinshang/canyu/www/documentroot/file",
      					"maxSize" => 6000, 
      					"allowFiles" => array("xls","gif", "png", "jpg", "jpeg", "bmp")
  					);
                self::$_spritepath='/mnt/hgfs/centos/psone/sprite';
                self::$_logdir='/mnt/hgfs/centos/psone/logs';
        }
        /**
         * 日志目录
         * @return string
         */
        public static function getLogdir()
        {
        	return self::$_logdir;
        }
        /**
         * 获取MongoDB相关参数
         */
        public static function getMongoDbs($cnfdbname)
        {
        	return self::$_mongodb[$cnfdbname];
        }
		/**
		 * redis 服务器
		 * @return Ambigous <multitype:, multitype:string number >
		 */
        public  static function getRedisServer($cnfdbname)
        {
        	return self::$_redisdb[$cnfdbname];
        }
        /**
         * 框架路径
         * @return string
         */
        public static function getSpritePath()
        {
        	return self::$_spritepath;
        }
        /**
         * memcached服务器列表
         * @return array ip 端口号
         */
        public static function getMemcacheServers($cnfdbname)
        {
        	return self::$_memcached[$cnfdbname];
        }
        /**
         * 获取指定数据库相关配置
         * dsn
         * user
         * password
         * charset
         * @param string $cnfdbname 数据库配置节点名称
         * @return array
         */
        public static function getMysqldbs($cnfdbname)
        {
                return self::$_mysqldb[$cnfdbname];
        }
        /**
         * 上传配置信息包含
         * viewUrl 访问URL
         * savePath 保存路径
         * maxSize 文件大小上限
         * allowFiles 文件类型
         * @return array 
         */
        public static function getUploadConfig()
        {
        	return self::$_uploadconfig;
        }
}
