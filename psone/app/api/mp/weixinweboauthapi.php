<?php
namespace app\api\mp;


class WeixinWebOauthApi
{
	
	
	const GET_AUTH_APPID='wx41c27f71a5bba57f';
	const GET_AUTH_APPSECRET='d0cd22ab1b835bb42e077decd407aaf3';
	const GET_AUTH_CODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize";
	const GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token";
	const GET_USER_INFO="https://api.weixin.qq.com/sns/userinfo";
	const GET_REFRESH_TOKEN="https://api.weixin.qq.com/sns/oauth2/refresh_token";
	const GET_CHECK_TOKEN="https://api.weixin.qq.com/sns/auth";
	const GET_CALLKACK_URL="http://hotmen.yoka.com/api/oauth2";
	
	
	/**
	 * 获取code
	 * @param string $needcheck 如果为真则需要用户授权 才能获取用户数据 否则只能获取用户openid
	 * 
	 */
	function getCode($needcheck=false)
	{
		$scope ='snsapi_userinfo';
		//-------生成唯一随机串防CSRF攻击
		$state = md5(uniqid(rand(), TRUE));
		setcookie('weixin_oauth_state',$state,time()+600,'/');
		$keysArr = array(
				"appid" => self::GET_AUTH_APPID,
				"redirect_uri" => self::GET_CALLKACK_URL,
				"response_type"=>'code',
				"scope" => $scope,
				"state" => $state
		);
		$url=self::GET_AUTH_CODE_URL.'?'.http_build_query($keysArr).'#wechat_redirect';
		header("Location:$url");
	}
	/**
	 * 获取用户acccesstoken
	 * @return mixed
	 */
	function getAccess_Token()
	{
	
		$state = @$_COOKIE['weixin_oauth_state'];
		if(empty($state) || $state!=$_GET['state'])
		{
			//syslog(LOG_ERR, '微信页面登录, state不符疑似攻击');
			die('系统异常');
		};
		$code=$_GET['code'];
	
		$keysArr = array(
				"appid" => self::GET_AUTH_APPID,
				"secret" => self::GET_AUTH_APPSECRET,
				"code"=>$code,
				"grant_type" => 'authorization_code'
		);
		$url=self::GET_ACCESS_TOKEN_URL.'?'.http_build_query($keysArr);
		$rel=self::accessUrl($url);
		$relarray=json_decode($rel,true);
		if(array_key_exists('errcode', $relarray))
		{
			print_r($relarray);exit;
			//die($relarray);
			//syslog(LOG_ERR, '获取AccessToken出错'.$rel);
			header('location:index.php');
			exit;
		}
		return $relarray;
	}
	function refreshToken($refresh_token)
	{
		$keysArr = array(
				"appid" => self::GET_AUTH_APPID,
				"grant_type" => 'refresh_token ',
				"refresh_token"=>$code
		);
		$url=self::GET_ACCESS_TOKEN_URL.'?'.http_build_query($keysArr);
		$rel=self::accessUrl($url);
		$relarray=json_decode($rel,true);
		if(array_key_exists('errcode', $relarray))
		{
			syslog(LOG_ERR, '获取AccessToken出错'.$rel);
			die('系统异常');
		}
		return $relarray;
	}
	function getUserInfo($usre)
	{
		$keysArr = array(
				"access_token" => $usre['access_token'],
				"openid" => $usre['openid'],
				"lang"=>'zh_CN'
		);
		$url=self::GET_USER_INFO.'?'.http_build_query($keysArr);
		$rel=self::accessUrl($url);
		$relarray=json_decode($rel,true);
		if(array_key_exists('errcode', $relarray))
		{
			syslog(LOG_ERR, '获取AccessToken出错'.$rel);
			die('系统异常');
		}
		return $relarray;
	}
	function getCheckToken($usre)
	{
		$keysArr = array(
				"access_token" => $usre['access_token'],
				"openid" => $usre['openid']
		);
		$url=self::GET_CHECK_TOKEN.'?'.http_build_query($keysArr);
		$rel=self::accessUrl($url);
		$relarray=json_decode($rel,true);
		if($relarray['errcode']!=0)
		{
			syslog(LOG_ERR, '获取AccessToken出错'.$rel);
			die('系统异常xx');
		}
		return true;
	}
	function accessUrl($url) {
	
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec($ch);
		curl_close($ch);
	
		return $result;
	}

}