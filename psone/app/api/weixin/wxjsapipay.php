<?php
/**
 * 
 * 接口访问类，包含所有微信支付API列表的封装
 * 每个接口有默认超时时间（除提交被扫支付为10s，上报超时时间为1s外，其他均为6s）
 * @author widyhu
 *
 */
namespace app\api\weixin;
use app\service\LogSvc;

class WxJsApiPay
{
	//APPID：绑定支付的APPID（必须配置，开户邮件中可查看）
	protected $mchId;
	//MCHID：商户号（必须配置，开户邮件中可查看）
	protected $appId;
	//KEY：商户支付密钥，参考开户邮件设置（必须配置，登录商户平台自行设置）
	protected $key;
	//统一下单接口地址
	protected $uoUrl = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
	//查询订单接口地址
	protected $qoUrl = 'https://api.mch.weixin.qq.com/pay/orderquery';
	//关闭订单接口地址
	protected $coUrl = 'https://api.mch.weixin.qq.com/pay/closeorder';
	//错误信息
	public $error = '';
	
	
	
	public function __construct($appId,$key,$mchId){
// 		$this->key = 'hotmen65873333hotmen65873333yoka';
// 		$this->appId = 'wx41c27f71a5bba57f';
// 		$this->mchId = '1356368302';
		$this->appId = $appId;
		$this->key = $key;
		$this->mchId = $mchId;
	}
	
	
	
	
    /**
     * 
     * 统一下单
     * @param array $rData 接口参数(参考官方文档)
     * @param int $timeOut
     * @return 成功时返回，其他抛异常
     */
    public function unifiedOrder($rData, $timeOut = 6)
    {
    	//必填参数部分初始化
        $data = array(
        	'appid' => $this->appId,
        	'mch_id' => $this->mchId,
        	'nonce_str' => self::getNonceStr(),
        	'body' => '',
        	'out_trade_no' => '',
        	'total_fee' => '',
        	'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
        	'notify_url' => '',
        	'trade_type' => '',
        );
        $rData = array_merge($data,$rData);
        //设置签名
        $rData = $this->SetSign($rData);
        $xml = $this->ToXml($rData);
        //var_dump($xml);exit;
        $response = $this->postXmlCurl($xml, $this->uoUrl, false, $timeOut);
        $result = $this->Init($response);
        return $result;
    }
    
    
    
    
    /**
     *
     * 查询订单
     * @param array $rData 接口参数
     * @param int $timeOut
     * @return 成功时返回，其他抛异常
     */
    public function orderQuery($rData, $timeOut = 6)
    {
    	//必填参数部分初始化
    	$data = array(
    		'appid' => $this->appId,
    		'mch_id' => $this->mchId,
    		'nonce_str' => self::getNonceStr(),
    		'out_trade_no' => '',
    	);
    	
    	$rData = array_merge($data,$rData);
    	//设置签名
    	$rData = $this->SetSign($rData);
    	$xml = $this->ToXml($rData);
    	    
    	
    	$response = $this->postXmlCurl($xml, $this->qoUrl, false, $timeOut);
        $result = $this->Init($response);
        return $result;
    }
    
    
    
    /**
     *
     * 关闭订单
     *array $rData 接口参数
     * @param int $timeOut
     * @return 成功时返回，其他抛异常
     */
    public static function closeOrder($rData, $timeOut = 6)
    {
    	//必填参数部分初始化
    	$data = array(
    		'appid' => $this->appId,
    		'mch_id' => $this->mchId,
    		'nonce_str' => self::getNonceStr(),
    		'out_trade_no' => '',
    	);
    	
    	$rData = array_merge($data,$rData);
    	//设置签名
    	$rData = $this->SetSign($rData);
    	$xml = $this->ToXml($rData);
    	    
    	
    	$response = $this->postXmlCurl($xml, $this->coUrl, false, $timeOut);
        $result = $this->Init($response);
        return $result;
    }
    
    
    
    /**
     *
     * 获取jsapi支付的参数
     * @param array $UnifiedOrderResult 统一支付接口返回的数据
     * @return json数据，可直接填入js函数作为参数
     */
    public function getJsApiParameters($UnifiedOrderResult)
    {
    	if(!array_key_exists("appid", $UnifiedOrderResult)
    			|| !array_key_exists("prepay_id", $UnifiedOrderResult)
    			|| $UnifiedOrderResult['prepay_id'] == "")
    	{
    		$this->error = ('参数错误');
    		return false;
    	}
    	//数据组装
    	$rData = array(
    		'appId' => $UnifiedOrderResult["appid"],
    		'timeStamp' => time(),
    		'nonceStr' => self::getNonceStr(),
    		'package' => "prepay_id=" . $UnifiedOrderResult['prepay_id'],
    		'signType' => 'MD5',
    	);
    	$rData = $this->SetSign($rData);
//     	$rData['paySign'] = $rData['sign'];
//     	unset($rData['sign']);
//     	$rData = json_encode($rData);
    	return $rData;
    }
    
    
    
    /**
     * 将xml转为array
     * @param string $xml
     * @throws WxPayException
     */
    public function Init($xml)
    {
    	$data = $this->FromXml($xml);
    	//fix bug 2015-06-29
    	if($data['return_code'] != 'SUCCESS'){
    		$this->error = $data['return_msg'];
    		return false;
    	}
    	if($this->CheckSign($data)){
    		return $data;
    	}
    	return false;
    }
    
    
    /**
     * 设置签名，详见签名生成算法
     * @param string $value
     **/
    public function SetSign($data)
    {
    	$sign = $this->MakeSign($data);
    	$data['sign'] = $sign;
    	return $data;
    }



    /**
     *
     * 检测签名
     */
    public function CheckSign($data)
    {
    	//fix异常
    	if(!isset($data['sign'])){
    		$this->error = '签名参数错误！';
    		return false;
    	}
    
    	$sign = $this->MakeSign($data);
    	if($data['sign'] == $sign){
    		return true;
    	}
    	$this->error = '签名错误！';
    	return false;
    }
    
    
    /**
     * 生成签名
     * @return 签名，本函数不覆盖sign成员变量
     */
    public function MakeSign($data)
    {
    	//签名步骤一：按字典序排序参数
    	ksort($data);
    	$string = $this->ToUrlParams($data);
    	//签名步骤二：在string后加入KEY
    	$string = $string . "&key=".$this->key;
    	//签名步骤三：MD5加密
    	$string = md5($string);
    	//签名步骤四：所有字符转为大写
    	$result = strtoupper($string);
    	return $result;
    }
    
    
    
    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams($data)
    {
    	$buff = "";
    	foreach ($data as $k => $v)
    	{
    		if($k != "sign" && $v != "" && !is_array($v)){
    			$buff .= $k . "=" . $v . "&";
    		}
    	}
    
    	$buff = trim($buff, "&");
    	return $buff;
    }
    
    
    
    /**
     * 输出xml字符
     * 
     **/
    public function ToXml($data)
    {
    	if(!is_array($data)
    			|| count($data) <= 0)
    	{
    		$this->error = '数组数据异常！';
    		return false;
    	}
    	 
    	$xml = "<xml>";
    	foreach ($data as $key=>$val)
    	{
    		if (is_numeric($val)){
    			$xml.="<".$key.">".$val."</".$key.">";
    		}else{
    			$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
    		}
    	}
    	$xml.="</xml>";
    	return $xml;
    }
   
    
    /**
     * 
     * 产生随机字符串，不长于32位
     * @param int $length
     * @return 产生的随机字符串
     */
    public static function getNonceStr($length = 32) 
    {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";  
        $str ="";
        for ( $i = 0; $i < $length; $i++ )  {  
            $str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);  
        } 
        return $str;
    }
    
    
    
    /**
     * 将xml转为array
     * @param string $xml
     * @throws WxPayException
     */
    public function FromXml($xml)
    {
    	if(!$xml){
    		$this->error = 'xml数据异常！';
    		return false;
    	}
    	//将XML转为array
    	//禁止引用外部xml实体
    	libxml_disable_entity_loader(true);
    	$resutl = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
    	return $resutl;
    }
    
    

    /**
     * 以post方式提交xml到对应的接口url
     * 
     * @param string $xml  需要post的xml数据
     * @param string $url  url
     * @param bool $useCert 是否需要证书，默认不需要
     * @param int $second   url执行超时时间，默认30s
     * @throws WxPayException
     */
    private function postXmlCurl($xml, $url, $useCert = false, $second = 30)
    {       
    	LogSvc::WriteLog('微信支付接口请求xml',$xml,'wxpay');
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);
        
        //如果有配置代理这里就设置代理
//         if(WxPayConfig::CURL_PROXY_HOST != "0.0.0.0" 
//             && WxPayConfig::CURL_PROXY_PORT != 0){
//             curl_setopt($ch,CURLOPT_PROXY, WxPayConfig::CURL_PROXY_HOST);
//             curl_setopt($ch,CURLOPT_PROXYPORT, WxPayConfig::CURL_PROXY_PORT);
//         }
        curl_setopt($ch,CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,TRUE);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);//严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    
//         if($useCert == true){
//             //设置证书
//             //使用证书：cert 与 key 分别属于两个.pem文件
//             curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
//             curl_setopt($ch,CURLOPT_SSLCERT, WxPayConfig::SSLCERT_PATH);
//             curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
//             curl_setopt($ch,CURLOPT_SSLKEY, WxPayConfig::SSLKEY_PATH);
//         }
        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        LogSvc::WriteLog('微信支付接口响应xml',$data,'wxpay');
        //返回结果
        if($data){
            curl_close($ch);
            return $data;
        } else { 
            $error = curl_errno($ch);
            curl_close($ch);
            $this->error = 'curl出错，错误码:$error';
            return false;
        }
    }
    
   
}

