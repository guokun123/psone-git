<?php
/**
 * 获取微信jssdkwx.config({})配置参数
 * @author bao
 *
 */
namespace app\api\weixin;
class wxJsSdk{
	
	protected $appId;
	protected $appSecret;
	protected $tokenUrl = 'https://api.weixin.qq.com/cgi-bin/token?';
	protected $ticketUrl = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?';
	public $error;
	public $errCode;
	
	
	public function __construct($appId,$appSecret){
		$this->appId = $appId;
		$this->appSecret = $appSecret;
	}
	
	
	/**
	 * 获取access_token
	 * 
	 */
    public function getAccessToken(){
    	$data = array(
    		'grant_type' => 'client_credential',
    		'appid' => $this->appId,
    		'secret' => $this->appSecret
    	);
    	$url = $this->tokenUrl.http_build_query($data);
    	$data = $this->requestCrul($url);
    	if($data){
    		if(isset($data['access_token'])){
    			return $data['access_token'];
    		}else{
    			$this->error = $data['errmsg'];
    			$this->errCode = $data['errcode'];
    			return false;
    		}
    	}
    	
    }
    
    
    /**
     * 获取jsapi_ticket
     * jsapi_ticket（有效期7200秒，开发者必须在自己的服务全局缓存jsapi_ticket）
     */
    public function getJsapiTicket($accessToken = null){
    	if(is_null($accessToken)){
    		$accessToken = $this->getAccessToken();
    		if(!$accessToken){
    			return false;
    		}
    	}
    	$data = array(
    		'access_token' => $accessToken,
    		'type' => 'jsapi',
    	);
    	$url = $this->ticketUrl.http_build_query($data);
    	$data = $this->requestCrul($url);
    	if($data){
    		if($data['errcode'] == 0){
    			return $data['ticket'];
    		}else{
    			$this->error = $data['errmsg'];
    			$this->errCode = $data['errcode'];
    			return false;
    		}
    	}
    }
    
    
    /**
     * 获取wxjssdk配置信息
     * 签名用的noncestr和timestamp必须与wx.config中的nonceStr和timestamp相同,所以要从外面传
     * @param $noncestr 随机字符串 (可以通过$this->getNonceStr()获取)
     * @param $timestamp 时间戳
     * @param $url 调用JS接口页面的完整URL，不包含#及其后面部分
     * @param $jsapiTicket $jsapiTicket参数(可以通过$this->getJsapiTicket())因为该参数涉及到全局缓存,不建议在本方法内部获取
     */
    public function getConfInfo($url,$jsapiTicket){
    	$noncestr = $this->getNonceStr();
    	$timestamp = time();
    	$data = array(
    		'appId' => $this->appId,
    		'timestamp' => $timestamp,
    		'noncestr' => $noncestr,
    	);
    	$sign = $this->getSign($noncestr, $timestamp, $url, $jsapiTicket);
    	$data['signature'] = $sign;
    	return $data;
    }
    
    
    
    /**
     * 获取签名
     * 签名用的noncestr和timestamp必须与wx.config中的nonceStr和timestamp相同,所以要从外面传
     * @param $noncestr 随机字符串 (可以通过$this->getNonceStr()获取)
     * @param $timestamp 时间戳
     * @param $url 调用JS接口页面的完整URL，不包含#及其后面部分
     * @param $jsapiTicket $jsapiTicket参数(可以通过$this->getJsapiTicket())因为该参数涉及到全局缓存,不建议在本方法内部获取
     */
    public function getSign($noncestr,$timestamp,$url,$jsapiTicket){
    	$data = array(
    		'noncestr' => $noncestr,
    		'jsapi_ticket' => $jsapiTicket,
    		'timestamp' => $timestamp,
    		'url' => $url
    	);
    	$sign = $this->MakeSign($data);
    	return $sign;
    }
    
    
    
    /**
     * 生成签名
     * @return 签名，本函数不覆盖sign成员变量
     */
    public function MakeSign($data)
    {
    	//签名步骤一：按字典序排序参数
    	ksort($data);
    	$string = $this->ToUrlParams($data);
    	//签名步骤二：MD5加密
    	$string = sha1($string);
    	
    	return $string;
    }
    
    
    
    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams($data)
    {
    	$buff = "";
    	foreach ($data as $k => $v)
    	{
    		if($k != "sign" && $v != "" && !is_array($v)){
    			$buff .= $k . "=" . $v . "&";
    		}
    	}
    
    	$buff = trim($buff, "&");
    	return $buff;
    }
    
    
    
    /**
     *
     * 产生随机字符串，不长于32位
     * @param int $length
     * @return 产生的随机字符串
     */
    public static function getNonceStr($length = 32)
    {
    	$chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    	$str ="";
    	for ( $i = 0; $i < $length; $i++ )  {
    		$str .= substr($chars, mt_rand(0, strlen($chars)-1), 1);
    	}
    	return $str;
    }
    
    
    public function requestCrul($url,$timeout=30){
    	//初始化curl
    	$ch = curl_init();
    	//设置超时
    	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    	curl_setopt($ch, CURLOPT_URL, $url);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,FALSE);
    	curl_setopt($ch, CURLOPT_HEADER, FALSE);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    	//         if(WxPayConfig::CURL_PROXY_HOST != "0.0.0.0"
    	//             && WxPayConfig::CURL_PROXY_PORT != 0){
    	//             curl_setopt($ch,CURLOPT_PROXY, WxPayConfig::CURL_PROXY_HOST);
    	//             curl_setopt($ch,CURLOPT_PROXYPORT, WxPayConfig::CURL_PROXY_PORT);
    	//         }
    	//运行curl，结果以jason形式返回
    	$data = curl_exec($ch);
    	if($data){
    		curl_close($ch);
    		$data = json_decode($data,true);
    		return $data;
    	} else {
    		$error = curl_errno($ch);
    		curl_close($ch);
    		$this->error = 'curl出错，错误码:$error';
    		return false;
    	}
    }
}