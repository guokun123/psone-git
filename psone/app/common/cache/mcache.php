<?php
namespace app\common\cache;


class MCache
{
    private static $_memcached;
    private function __construct(){}
    public static function getInstance()
    {
        if(empty(self::$_memcached))
        {
            self::$_memcached = new \Memcached();
            $servers = \SpriteConfig::getMemcacheServers('MEMMAIN');
            self::$_memcached -> addServers($servers);
        }
        return self::$_memcached;
    }
}