<?php
namespace app\common\cache;

use \SpriteConfig;
/**
 * redis 缓存
 *
 * @author 23003
 *
 */
class RCache {

    private static $_redis;
    public function __construct(){}
    /**
     * 配置文件里配置
     * $servers = ['host'=>'127.0.0.1','port'=>6379,'user'='user','pwd'=>'password'];
     * @param unknown $servers 服务器信息
     * @return \Memcached
     */
    public static function getInstance()
    {
        if (empty(self::$_redis)) {
            self::$_redis = new \Redis();
            $server=\SpriteConfig::Getredisserver('REDISMAIN');
            if(isset($_GET['_debug']) && $_GET['_debug'==1])
            {
                var_dump($server);
            }

            if(empty($server))
            {
                die('Redis配置信息错误');
            }
            $host=$server['host'];
            $port=$server['port'];
            $pwd=$server['pwd'];

            if (self::$_redis->pconnect($host, $port) == false) {
                die(self::$_redis->getLastError());
            }

            if (self::$_redis->auth($pwd) == false) {
                die(self::$_redis->getLastError());
            }


        }
        return self::$_redis;
    }
    /**
     * redis缓存
     * @param unknown $key
     * @param unknown $time
     * @param unknown $get_data_func
     * @param unknown $func_params
     * @return mixed
     */
    public static function access_cache($key, $time, $get_data_func, $func_params=array()) {
        self::getInstance();
        $data = self::$_redis->get($key);

        if (empty($data) || isset($_GET['_refresh'])) {
            $data = call_user_func_array($get_data_func, $func_params);
            if($time>0)
            {
                self::$_redis->set($key, $data, $time);
            }else
            {
                self::$_redis->set($key, $data);
            }

        }

        return $data;
    }
}
