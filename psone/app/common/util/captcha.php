<?php
namespace app\common\util;

use app\common\cache\Mcache;

class Captcha {
    
    //从memcache取验证码
    public static function getCaptcha() {
        $k = self::getCaptchaKey();
        if (!$k)
            return 'user no login';
        
        $r = MCache::getInstance()->get($k);
        MCache::getInstance()->set(self::getCaptchaKey(), 'com.yoka'.rand(1, 1000), 3600);
        
        return $r;
    }

    //保存验证码到memcache
    public static function setCaptcha($text) {
        return MCache::getInstance()->set(self::getCaptchaKey(), $text, 3600); //一小时
    }

    private static function getCaptchaKey() {
        if (empty($_COOKIE['KM_PASSPORT_MEMBER']))
            return '';      

        parse_str($_COOKIE['KM_PASSPORT_MEMBER'], $userInfo);
        if (empty($userInfo['uid']))
            return '';
        else
            return md5($userInfo['uid'].'captcha');
    }

}
