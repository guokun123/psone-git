<?php
namespace app\common\util;
/**
 * @author jin
 *	IP相关
 * ip.php
 * 2014年12月30日 下午3:11:52
 */
class IP 
{
	

	public static function getuserip()
	{
			try{
	
			$ips='';
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
				$ips=$_SERVER["HTTP_X_FORWARDED_FOR"];
			else if (isset($_SERVER["REMOTE_ADDR"]))
				$ips= $_SERVER["REMOTE_ADDR"];
			else if (isset($_SERVER["HTTP_CLIENT_IP"]))
				$ips=$_SERVER["HTTP_CLIENT_IP"];
			else if (isset($_SERVER["HTTP_X_FORWARDED"]))
				$ips= $_SERVER["HTTP_X_FORWARDED"];
			else if (isset($_SERVER["HTTP_FORWARDED"]))
				$ips= $_SERVER["HTTP_FORWARDED"];
	
			return $ips;
		}
		catch(Exception $e)
		{
			return "0";
		}
	}
	
	
	public static function get_real_ip() {
		try{
	
			$ips='';
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
				$ips=$_SERVER["HTTP_X_FORWARDED_FOR"];
			else if (isset($_SERVER["REMOTE_ADDR"]))
				$ips= $_SERVER["REMOTE_ADDR"];
			else if (isset($_SERVER["HTTP_CLIENT_IP"]))
				$ips=$_SERVER["HTTP_CLIENT_IP"];
			else if (isset($_SERVER["HTTP_X_FORWARDED"]))
				$ips= $_SERVER["HTTP_X_FORWARDED"];
			else if (isset($_SERVER["HTTP_FORWARDED"]))
				$ips= $_SERVER["HTTP_FORWARDED"];
	
			$keyip = preg_split("/[s,]+/", $ips);
	
			foreach ($keyip as $v) {
	
				if(strstr(substr($v,0,3),"10.") ==false && strstr(substr($v,0,7),"192.168") ==false)
					return $v;
			}
			return "0";
		}
		catch(Exception $e)
		{
			return "0";
		}
	
	}
}