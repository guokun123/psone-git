<?php
/**
 * 
 * @author li.weiwei@163.com
 * @date 2013-03-28
 */
namespace app\common\util;

use sprite\lib\ShortUrl;
use app\dao\short as shortDao;

class short {
	const PERFIX = 'http://fan.yoka.com/s/';
	
	public static function generate($url) {
		if (!$url)
			return false;
		
		$short = shortDao::findOne(array('url'=>$url));
		if ($short)
			return self::PERFIX.$short->short;
		
		$shorts = ShortUrl::geterateAll($url);
		foreach ($shorts as $v) {
			$p = new shortDao();
			try {
				$p->short = $v;
				$p->url = $url;
				$p->utime = $p->now();
				$r = $p->save(array('safe'=>true));
			} catch (\MongoCursorException $e) {
				continue;
			}
			if ($r)
				return self::PERFIX.$v;
		}
		
		return false;
	}
	
	public static function get($shortUrl) {
		$p = shortDao::findOne(array('short'=>$shortUrl));
		
		return $p? $p->url:'';
	}
}