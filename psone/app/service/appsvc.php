<?php
/**
 * @author	widewolf
* date		2012-11-10
*/

namespace app\service;

use app\service\baseSvc;
use app\dao\system\System_Manager;
use app\service\MCacheSvc;
use app\dao\mysql\paper\Paper_APP;

class AppSvc extends baseSvc 
{
	public function getall()
	{
		$dao=new Paper_APP();
		$rows=$dao->searchAll([], '1=1');
		return $rows;
	}
	/**
	 * 通过KEY获取APP信息
	 * @param unknown $appkey
	 * @return unknown|NULL
	 */
	public function getByAppkey($appkey)
	{
		$dao=new Paper_APP();
		$rows=$dao->findByField('appkey', $appkey);
		if(count($rows)>0)
		{
			return $rows[0];
		}
		return null;
	}
}