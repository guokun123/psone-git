<?php
/**
 * 服务基类 有统计的正常输出和错误输出
 * @author imlivv@gmail.com
 * @date 2012-11-12
 */

namespace app\service;

use sprite\lib\Debug;

class BaseSvc {
	
	private $error;
	private $return;
	protected static $_cache_time = 600; //10分钟
	
	public function __construct() {
		
	}
	
	protected function output($error=null, $return=null) {
		if ($error)
			$this->error = $error;
		if ($return)
			$this->return = $return;
		
		//Debug::log('srv error:', $this->error?$this->error:'none');
		return array($this->error, $this->return);
	}
	
	protected function setResult($r) {
		$this->return = $r;
	}
	
	protected function setError($e) {
		$this->error = $e;
	}
	

	/**
	 * 缓存以 _with_cache结尾的方法结果，
	 * 如果第一个参数是 cache_time=(%d) 就缓存%d秒，否则取默认值
	 */
	public function __call($func, $params) {
		if (substr($func, -11) == '_with_cache') {
			$cache_time = static::$_cache_time;
			$relFunc = substr($func, 0, strlen($func)-11);
			$cache_time_params = isset($params[0])? $params[0]:null;
			$key = md5($func.serialize($params));

			if (is_string($cache_time_params) && substr($cache_time_params, 0, 11) == 'cache_time=') {
				$cache_time = intval(substr($cache_time_params, 11));
				array_shift($params); //第一个参数是缓存时间不是函数用的，去除
			}
			
			$data = MCacheSvc::access_cache($key, $cache_time, array($this, $relFunc), $params);
			return $data;
		}
	}
}