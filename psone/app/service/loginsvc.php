<?php
namespace app\service;
use app\dao\mysql\user;

//登陆方法
class LoginSvc
{
    public static function login($request)
    {
        $email = $request ->email;
        $psd = $request -> password;
        $dao = new user\user_base;
        $userinfo = $dao -> findAll(['_email'=>$email],'_email= :_email','1',['_id','_pwd','_phonenum','_weixin',]);
        if($userinfo)
        {
            if($userinfo['_psd'] == self::md5_salt($psd))
            {
                $memchche = new MCacheSvc;
                $memchche -> set('user_base_id',$userinfo['_id']);
                $memchche -> set('user_base_email',$email);
                return array('200',$userinfo);
            }
            else
                return array('-101','密码不正确');
        }
        else
            return array('-100','用户名不存在');
    }
    //加密方法
    public static function md5_salt($da)
    {
        $salt = 'PS-ONE-IS-BSET';
        $data = md5($da.$salt);
        return $data;
    }
}
