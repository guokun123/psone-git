<?php
/**
 * 防止有大量并发的缓存服务存取服务
 * @author li.weiwei@163.com
 * @date 2013年6月20日
 */

namespace app\service;

use sprite\lib\MCache;
use \SpriteConfig;

/**
 * Memcached缓存服务
 * @author 23003
 *
 */
class MCacheSvc {
	
	private static $memcache;
	
	/*private function __construct() {}*/
	
	public static function getInstance() {
		if (!self::$memcache)
		{
			self::$memcache = MCache::getInstance();
		}
		return self::$memcache;
	}
	
	/**
	 * 查看服务器状态
	 * @return unknown
	 */
	public static function getserstatus()
	{
		self::getInstance();
		$data = self::$memcache->getStats ();
		return $data;
	}
	
	
	/**
	 * 读缓存
	 * @param string $key
	 * @return Ambigous <NULL, unknown>
	 */
	public static function get($key){
		self::getInstance();
		$data = self::$memcache->get($key);
		return $data?$data:null;
	}
	/**
	 * 写缓存
	 * @param string $key
	 * @param data $data
	 * @param timestamp $time=0
	 * @return bool 是否成功
	 */
	public static function set($key,$data,$time=0)
	{
		self::getInstance();
		$iscache = self::$memcache->set($key, $data, $time);
		return $iscache;
	}
	/**
	 * 获取KEY集合
	 */
	public static function getallkeys()
	{
		self::getInstance();
		$data = self::$memcache->getAllKeys();
		return $data?$data:null;
	}
	
	/**
	 * 删除指定前缀的cache
	 */
	public static function delbyprefix($prefix)
	{
		self::getInstance();
		$data = self::getallkeys();
		$count=0;
		foreach ($data as $key)
		{
			$r=strpos($key, $prefix);
			if($r==0)
			{
				self::del($key);
				$count++;
			}
		}
		return $count;
	}
	
	/**
	 * 删除
	 * @param unknown $key
	 */
	public static function del($key)
	{
		self::getInstance();
		$iscache=$data = self::$memcache->get($key);
	
		if ($data)
			$iscache= self::$memcache->delete($key);
	
		return $iscache;
	}
	/**
	 * notice 本缓存不会存 [0 false null] 不加锁
	 * @param unknown $key
	 * @param unknown $time
	 * @param unknown $get_data_func
	 * @param unknown $func_params
	 * @return mixed
	 */
	public static function access_cache($key, $time, $get_data_func, $func_params=array()) {
		self::getInstance();
		$data = self::$memcache->get($key);
	
		if (empty($data) || isset($_GET['_refresh'])) {
			$data = call_user_func_array($get_data_func, $func_params);
			self::$memcache->set($key, $data, $time);
		}
		
		return $data;
	}
	

	/**
	 * notice 本缓存不会存 [0 false null] 加锁
	 * @param unknown $key
	 * @param unknown $time
	 * @param unknown $get_data_func
	 * @param unknown $func_params
	 * @return unknown|mixed
	 */
	public static function access_cache_x($key, $time, $get_data_func, $func_params=array()) {
		self::getInstance();
		$data = self::$memcache->get($key);
		
		if ($data && empty($_GET['_refresh']))
			return $data;
		else
			self::$memcache->delete($key);
		
		//防止并发取缓存
		if(self::$memcache->add($key, null)) {
			$data = call_user_func_array($get_data_func, $func_params);
			if (!empty($data))
				self::$memcache->set($key, $data, $time);

		} else {
			for($i=0; $i<20; $i++) { //4秒没有反应，就出白页吧，系统貌似已经不行了
				sleep(0.2);
				$data = self::$memcache->get($key);
				if ($data !== false)
					break;

			}
		}
		
		return $data;
	}
		
}