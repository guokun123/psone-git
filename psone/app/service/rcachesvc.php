<?php
/**
 * 防止有大量并发的缓存服务存取服务
 * @author li.weiwei@163.com
 * @date 2013年6月20日
 */

namespace app\service;

use sprite\lib\RCache;


/**
 * redis缓存服务
 * @author 23003
 *
 */
class RCacheSvc {
	
	private static $redis;
	
	private function __construct() {}
	
	public static function getInstance() {
		if (!self::$redis)
		{
			self::$redis = RCache::getInstance();
		}
		return self::$redis;
	}
	
	
	
		
}