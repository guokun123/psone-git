<?php
/**
 * @author jin
 *
 * functionsvc.php
 * @date 2015年1月4日 下午3:44:12
 */
namespace app\service\system;

use app\dao\mysql\system\System_Function;
use app\service\baseSvc;

/**
 * 功能服务（菜单服务）
 * @author jin
 *
 */
class FunctionSvc extends baseSvc
{
	use \app\tools\Util;
	
	/**
	 * 获得用户权限字典用于后台权限验证
	 * @param unknown $uid
	 * @return Ambigous <\app\service\Ambigous, unknown>
	 * 长期缓存
	 */
	public static  function managerpermissionmap($uid)
	{
		$managerfunMap=array();
		$managerfunMap = CacheSvc::get ( 'system_functions_manager_map_'.$uid);
		if (! $managerfunMap)
		{
			$managerfunArray=FunctionSvc::managerpermission($uid);
			foreach ($managerfunArray as $siteid=>$sitefun)
			{

				foreach ($sitefun as $funid=>$fun)
				{
					foreach ($fun['sub'] as $subfunid=>$subfun)
					{
						$managerfunMap[$siteid][$fun['name']][]=$subfun['name'];
					}
				}
			}
			if(!empty($managerfunMap))
			{
				CacheSvc::set ( 'system_functions_manager_map_'.$uid , $managerfunMap ,0);
			}
		}
		return  $managerfunMap;
	}
	/**
	 * 获取用户权限 用于设置权限功能
	 * @param unknown $uid
	 * @return Ambigous <array, \app\service\Ambigous>
	 * 长期缓存
	 */
	public static function  managerpermission($uid)
	{
		$managerfunArray=array();
		$managerfunArray = CacheSvc::get ( 'system_functions_manager_'.$uid);
		if (! $managerfunArray) 
		{
			$permdao=new System_Manager_Permission();
			$row=$permdao->find($uid);

			$funArray=json_decode($row['permission'],true);

			foreach ($funArray as $siteid=>$sitefun)
			{
				foreach ($sitefun as $key=>$val)
				{
					$managerfunArray[intval($siteid)][intval($key)]=$val;
					unset($managerfunArray[intval($siteid)][intval($key)]['sub']);
					
					foreach ($val['sub'] as $skey=>$sval)
					{
						$managerfunArray[intval($siteid)][intval($key)]['sub'][intval($skey)]=$sval;
					}
					
				}
			}
			if(!empty($managerfunArray))
			{
				CacheSvc::set ( 'system_functions_manager_'.$uid , $managerfunArray ,0);
			}
		}
		return  $managerfunArray;
	}
	

	/**
	 * 全部功能列表
	 * @return \app\service\Ambigous
	 */
	public function getAllFunctions()
	{
		$funArray=array();
		$fundao=new System_Function();
		$binds=["pid"=>0,"type"=>0,"status"=>1];
		$sqlwhere='pid=:pid and type=:type and status=:status';
		$controlllist=$fundao->searchAll($binds,$sqlwhere,0);
		foreach ($controlllist as $_c)
		{
			$subfunArray=array();
			$funArray[$_c['id']]['ismenu']=$_c['ismenu'];
			$funArray[$_c['id']]['icon']=$_c['icon'];
			$funArray[$_c['id']]['name']=$_c['name'];
			$funArray[$_c['id']]['description']=$_c['description'];
			$binds['pid']=$_c['id'];
			$binds['type']=1;
			$actionlist=$fundao->searchAll($binds, $sqlwhere,0);
			foreach ($actionlist as $_a)
			{
				$subfunArray[$_a['id']]['ismenu']=$_a['ismenu'];
				$subfunArray[$_a['id']]['icon']=$_a['icon'];
				$subfunArray[$_a['id']]['name']=$_a['name'];
				$subfunArray[$_a['id']]['description']=$_a['description'];
			}
			$funArray[$_c['id']]['sub']=$subfunArray;
		}
		return $funArray;
	}
	public function getFunctionsList()
	{
		$funArray=array();
		$fundao=new System_Function();
		$binds=["pid"=>0,"type"=>0];
		$sqlwhere='pid=:pid and type=:type and status=:status';
		$controlllist=$fundao->searchAll($binds,$sqlwhere,0);
		foreach ($controlllist as $_c)
		{
			$subfunArray=array();
			$funArray[$_c['id']]['ismenu']=$_c['ismenu'];
			$funArray[$_c['id']]['icon']=$_c['icon'];
			$funArray[$_c['id']]['name']=$_c['name'];
			$funArray[$_c['id']]['description']=$_c['description'];
			$binds['pid']=$_c['id'];
			$binds['type']=1;
			$actionlist=$fundao->searchAll($binds, $sqlwhere,0);
			foreach ($actionlist as $_a)
			{
				$subfunArray[$_a['id']]['ismenu']=$_a['ismenu'];
				$subfunArray[$_a['id']]['icon']=$_a['icon'];
				$subfunArray[$_a['id']]['name']=$_a['name'];
				$subfunArray[$_a['id']]['description']=$_a['description'];
			}
			$funArray[$_c['id']]['sub']=$subfunArray;
		}
		return $funArray;
	}
}