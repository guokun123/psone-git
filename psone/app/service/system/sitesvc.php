<?php
/**
 * @author jin
 *
 * sitesvc.php
 * @date 2014年12月30日 下午4:11:52
 */
namespace app\service\system;

use app\service\baseSvc;
use app\dao\system\System_Site;
use app\service\CacheSvc;

class SiteSvc extends baseSvc 
{
	/**
	 * 站点所有信息列表
	 * @return \app\service\Ambigous
	 */
	public static function AllSiteList()
	{
		$siteArray = CacheSvc::get ( 'system_sitelist');
		if (! $siteArray) {
	
			$sitedao=new System_Site();
			$siteArray=$sitedao->searchAll([],[], '1', '', '', ' id asc');
			$siteArray?CacheSvc::set ( 'system_sitelist' , $siteArray ,60 * 60 * 24):null;
		}
		return $siteArray;
	}
	/**
	 * 站点MAP id name 用于显示
	 * @return Ambigous <\app\service\Ambigous, unknown>
	 */
	public static function SiteMap()
	{
		$sites = CacheSvc::get ( 'system_sitemap');
		if (! $sites) {
	
			$sitedao=new System_Site();
			$siteArray=$sitedao->searchAll([],[], '1', '', '', ' id asc');
			foreach ($siteArray as $site)
			{
				$sites[$site["id"]]=$site["sitename"];
			}
			CacheSvc::set ( 'system_sitemap' , $sites ,60 * 60 * 24);
		}
		return $sites;
	}
}