<?php
namespace app\service;


class TokenSvc
{
    //生成32位token
    public static function createNoncestr( $length = 32 )
    {
        $chars =  'p1s2o3n4e5i6s7bad8e9s0t123456';
        $str =  "";
        for( $i=0;$i<$length;$i++ ){
            $str .= substr( $chars, mt_rand( 0,strlen($chars)-1 ), 1 );
        }
        return $str;
    }
}