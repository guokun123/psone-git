<?php
namespace app\tools;

use app\common\cache\MCache;
use app\dao\system\System_Manager;
use app\service\LogSvc;

trait Util
{
	/**
	 * 模仿.NET的方法写的去除字符串最后一位指定字符
	 * @param unknown $str
	 * @param unknown $strEnd
	 * @return Ambigous <结果字符串, string>
	 */
	public function trimEnd($str,$strEnd)
	{
	
		$arrstr=str_split($str);
		if($arrstr[count($arrstr)-1]==$strEnd)
		{
			unset($arrstr[count($arrstr)-1]);
		}
		return implode('', $arrstr);
	
	}
    public function idFmt(array $objs, $key)
    {
        $out = array();

        if (count($objs) == 0)
            return $out;

        foreach ($objs as $o) {
           @ $out[] = $o->$key;
        }

        return $out;
    }

    public function keyFmt(array $objs, $key)
    {
        $out = array();

        if (count($objs) == 0)
            return $out;

        foreach ($objs as $o) {
            $out[$o->$key] = $o;
        }

        return $out;
    }

	public function getCurrentUserInfo() {
		$userInfo = [];
		$cookiename = 'KM_PASSPORT_MEMBER';
		if (empty($_COOKIE[$cookiename]))
			return $userInfo;
	
		$cookie_str = $_COOKIE [$cookiename];
		if (! empty ( $cookie_str )) { // cookie存在
	
			$array = explode ( "&", $cookie_str );
			$info = array ();
			foreach ( $array as $v ) {
				list ( $key, $value ) = explode ( '=', $v );
				$info [$key] = $value;
			}
			//支持sign和sign2 如果不需要sign了，可以删除
			$sign = md5 ( substr ( md5 ( $info ['pwd'] ), 0, 20 ) . 'YoKa' . substr ( md5 ( $info ['id'] ), 10, 20 ) );
			$sign2 = md5 ( substr ( md5 ( $info ['pwd'] ), 0, 20 ) . 'YoKa' . substr ( md5 ( $info ['id'] . $info ['uid'] . $info ['guid'] ), 10, 20 ) );
			if ($info ['uid'] > 0 && (@$sign == @$info ['sign'] || $sign2 == @$info ['sign2'])) {
				parse_str($_COOKIE['KM_PASSPORT_MEMBER'], $userInfo);
			}
		}
	
		return $userInfo;
	}
}
