<?php  
/**
 * 项目框架初始化文件
 * 本文件不包含任何配置信息，项目的所有配置在/config/local.config.php中
 * 开发期间，每个人拥有自己的local.config.php,在/config/用户名.local.config.php 文件中修改
 */
use sprite\logger\Logger;
\SpriteConfig::getInstance();

$d = date('Y-m-d');
$error_log_file=$_SERVER['HTTP_HOST']."-{$d}.log";;
$logFilePath = \SpriteConfig::getLogdir()."/$error_log_file";
ini_set('error_log',$logFilePath);
ini_set("memory_limit","4096M");
define('ROOT_PATH', __DIR__);

include \SpriteConfig::getspritepath().'/autoload.php';

$autoPath = __DIR__;
$path = get_include_path();
if (strpos($path.PATH_SEPARATOR, $autoPath.PATH_SEPARATOR) === false){
    set_include_path($path.PATH_SEPARATOR.$autoPath);
}
spl_autoload_register('spl_autoload');



