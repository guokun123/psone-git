<?php
namespace www\controller;

use app\service\LoginSvc;
use app\dao\mysql\user;
use app\service\RCacheSvc;
use app\service\TokenSvc;
use app\service\ValidatorSvc;

class Usercontroller extends BaseController
{
    public function auth($request, $response)
    {
        $login = new LoginSvc();
        $data = $login::login($request);
        if($data[0] == 200)
        {
            $tokensvc = new TokenSvc();
            $token = $tokensvc::createNoncestr();
            $redis = new RCacheSvc();
            if($token)
            {

            }

            $d = ['state'=>$data[0],"message"=>$data[1],"result"=>[
                "email"         =>  $data['_email'],
                "phonenum"     => "nonull",
                "realname"     =>"潘多拉",
                "weixin"       =>$data['_weixin'],
                "rolecategory"=>1 ,//1学生,2教务,3老师,4其他
                "rolec"        =>"momull", // 角色内容
                "infoherf"    =>"/student/*****"
            ]];
            $this->renderJson($d);
        }elseif($data[0] < 0 )
        {
            $this->renderJson($data);
        }
    }
    public function resgin($request, $response)
    {
        if($this->isPost())
        {
            //接收数据
            $email = $request -> email;
            $pwd = $request -> pwd;  //6-16
            $realname = $request -> realname;  //2-30
            $weixin = $request -> weixin;
            $role = $request -> role;
            $qq = $request -> qq;
            $birthday = $request -> birthday; //1988-11-14
            //做数据验证
            $data = array($email,$pwd,$realname,$weixin,$role,$qq,$birthday);
            $ValidatorSvc = new ValidatorSvc($data);
            $ValidatorSvc->setRule($email, 'required','email');
            $ValidatorSvc->setRule($pwd, array('length' => array(6,16)));
            $ValidatorSvc->setRule($realname,array('length' => array(3,20)));
            $ValidatorSvc->setRule($qq,array('length' => array(6,12)));
            $ValidatorSvc->setRule($weixin,'required');
            $ValidatorSvc->setRule($role,'required');
            $result = $ValidatorSvc->validate();
            if($result)
            {
                $da = new Urole_Base;
                $cbuid =  $da -> add(['_rolec'=>$role]);
                if($cbuid)
                {
                    $dao = new user\user_base;
                    $d = [
                        '_email'=>$email,
                        '_pwd'=>self::md5_salt($pwd),
                        '_realname'=>$realname,
                        '_role_id'=>$cbuid,
                        '_weixin'=>$weixin,
                        '_qq'=>$qq,
                        '_brithday'=>$birthday
                    ];
                    $res = $dao -> adds($d);
                    if($res)
                    {
                        $data = ['state'=>200,"message"=>'注册成功',"result"=>[]];
                        $this->renderJson($data);
                    }
                }
            }else
            {
                $r = $ValidatorSvc->getResultInfo();
                $this->renderJson($r);
            }
        }
    }
    //加密方法
    public static function md5_salt($da)
    {
        $salt = 'PS-ONE-IS-BSET';
        $data = md5($da.$salt);
        return $data;
    }
}

