<?php
/**
 * fan addmin 项目启动文件
 */
use sprite\mvc\App;
use sprite\mvc\Request;
use sprite\mvc\SmartyView;
use sprite\logger\Logger;



//echo 'aaaa';exit;
//初始项目相关框架
require __DIR__.'/../../init.php';

$app_namespace = 'www';
//初始化smarty配置
$smarty = SmartyView::getSmarty();


$smarty->left_delimiter = '{{';
$smarty->right_delimiter = '}}';
$smarty->template_dir = __DIR__ . '/../template/';
$smarty->cache_dir = ROOT_PATH . "/../tmp/$app_namespace/cache";
$smarty->compile_dir = ROOT_PATH . "/../tmp/$app_namespace/templates_c";
$smarty->registerClass('tpl', '\app\common\util\SmartyTpl'); //注册smarty 自定义函数类
$smarty->error_reporting = E_ALL & ~E_NOTICE & ~E_DEPRECATED;

$request = Request::getInstance();
$controller = $request->_c;
$action = $request->_a;


$mvc = new App($controller, $action,$app_namespace);

ob_start();
try {
	//echo 'mvc'.$controller, $action,$app_namespace;
	$mvc->run();
	//die('ccc');
} catch (\Exception $e) {
	$d = date('Y-m-d');
	$logFile = \SpriteConfig::getLogdir()."/site-{$app_namespace}-error-{$d}.log";

	$log=new Logger($app_namespace,$logFile);
	$log->error($e->getMessage());
	throw $e;
	//var_dump($e);
}

ob_end_flush();