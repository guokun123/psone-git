<?php
/**
 * Sprite配置文件
 */

class SpriteConfig
{
        private static $_mysqldb=array();
        private static $_mongodb=array();
        private static $_redisdb=array();
        private static $_memcached=array();
        private static $_uploadconfig=array();
		private static $_spritepath;
		private static $_logdir='';//日志目录
        public static function  getInstance()
        {
                self::$_mysqldb=array();
                self::$_mysqldb['YOKA_HOT']['dsn'] = 'mysql:host=localhost;port=3306;dbname=ps_one';
                self::$_mysqldb['YOKA_HOT']['user'] = 'test';
                self::$_mysqldb['YOKA_HOT']['password'] = '1234';
                self::$_mysqldb['YOKA_HOT']['charset'] = 'utf8';

                self::$_mysqldb['YOKA_HOT'][0]['dsn'] = 'mysql:host=localhost;port=3306;dbname=ps_one';
                self::$_mysqldb['YOKA_HOT'][0]['user'] = 'test';
                self::$_mysqldb['YOKA_HOT'][0]['password'] =  '1234';
                self::$_mysqldb['YOKA_HOT'][0]['charset'] = 'utf8';
                self::$_mysqldb['YOKA_HOT'][0]['weight'] = 9;
                
                self::$_mongodb=array();
                self::$_mongodb['YOKA_BRAND']['host'] = '127.0.0.1:27017';
                self::$_mongodb['YOKA_BRAND']['dbname'] = 'root';
                
                self::$_redisdb=array();
                self::$_redisdb['YOKA_REDIS']['host'] = '127.0.0.1';
                self::$_redisdb['YOKA_REDIS']['port'] = 6379;
                self::$_redisdb['YOKA_REDIS']['password'] = 'root';
                
                self::$_memcached=array();
                self::$_memcached['MAIN']=array('192.168.57.91', '11211');
                
                
                self::$_uploadconfig= array(
                		"viewUrl"=>'/upload',
     					"savePath" => "/home/haofenglong/YokaPersonas/upload/",
     					"maxSize" => 6000, //
     					"allowFiles" => array("xls","gif", "png", "jpg", "jpeg", "bmp")
 					);
                self::$_spritepath='/home/haofenglong/YokaPersonas/sprite';
                self::$_datapath='/home/haofenglong/YokaPersonas/webdata';
                self::$_logdir='/home/haofenglong/YokaPersonas/webdata';
        }
        /**
         * 日志目录
         * @return string
         */
        public static function getLogdir()
        {
        	return self::$_logdir;
        }
        /**
         * 获取MongoDB相关参数
         */
        public static function getMongoDbs($cnfdbname)
        {
        	return self::$_mongodbs[$cnfdbname];
        }
		/**
		 * redis 服务器
		 * @return Ambigous <multitype:, multitype:string number >
		 */
        public  static function getRedisServer($cnfdbname)
        {
        	return self::$_redisdb[$cnfdbname];
        }
        /**
         * 框架路径
         * @return string
         */
        public static function getSpritePath()
        {
        	return self::$_spritepath;
        }
        /**
         * memcached服务器列表
         * @return array ip 端口号
         */
        public static function getMemcacheServers($cnfdbname)
        {
        	return self::$_memcached[$cnfdbname];
        }
        /**
         * 获取指定数据库相关配置
         * dsn
         * user
         * password
         * charset
         * @param string $cnfdbname 数据库配置节点名称
         * @return array
         */
        public static function getMysqldbs($cnfdbname)
        {
                return self::$_mysqldbs[$cnfdbname];
        }
        /**
         * 上传配置信息包含
         * viewUrl 访问URL
         * savePath 保存路径
         * maxSize 文件大小上限
         * allowFiles 文件类型
         * @return array 
         */
        public static function getUploadConfig()
        {
        	return self::$_uploadconfig;
        }
}
