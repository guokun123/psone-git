<?php
namespace sprite\db\mongo;

use \MongoClient;
use \MongoCode;
use \MongoId;
use \MongoLog;
use \MongoCursorException;
use JsonSerializable;
use \Exception;
use \SpriteConfig;

abstract class BaseMongoRecord implements MongoRecord, JsonSerializable {
     /**
     * 定义schema信息，用来完善和约束字段
     * name => [type, default_value]
     * protected static $_schema = [
     *   '_id'     => ['object_id', null],
     *   'name'    => ['string', ''],
     *   'int_t'   => ['int', 0],
     *   'float_t' => ['float', 0.0],
     *   'auto_t'  => ['auto_increment', null],
     *   'list_t'  => ['list', [], 'int'],
     *   'dict_t'  => ['dict', null],
     *   'date_t'  => ['iso_date', null],
     *   'time_t'  => ['timestamp', null]
     *   ];
     */
    protected static $_schema = [];
    protected static $collectionName = null;
    public static $database = null;
    public static $connection = null;
    public static $findTimeout = 20000;

    //非静态变量以下划线开头，避免与数据实体冲突
    protected $_errors;
    protected $_new = false; //是不是新增的
    protected $_imperfect = false; //实体属性是不不完整的，用于判断update还是save
    
    
    public function __construct($attributes = array(), $new = true) { //信任从库里出来的数据，控制入库的数据符合格式
		$this->_new = $new;
		
		foreach ($attributes as $key => $value) {
			$this->$key = $value;
		}
		
		if (!BaseMongoRecord::$connection){
			try{
				$this->connect_mongodb();
			}catch (Exception $ex){
				die(' Error:connect to mongodb fail');
			}
		}
    }
    //取得数据库主库配置，由子类实现
    protected abstract function getMongoCfgName();
    private function _fmt() {
    	foreach (static::$_schema as $key => $value) {
    		if (isset($this->$key)) {
    			$v = $this->$key;
    		} else {
    			$v = null;
    			$this->_imperfect = true;  //更新是不能用save，会存入不完整数据，用update
    		}
    		$this->_set($key, $v);
    	}
    }

    private function _fmtImperfect() {
        $this->_imperfect = true;  //更新是不能用save，会存入不完整数据，用update
        foreach (static::$_schema as $key => $value) {
            if (isset($this->$key)) {
                $this->_set($key, $this->$key);
            }
        }
    }


    /**
     * 设置是否对当胆集合启用从库的查询，在复制集中默认为true,会自动将读操作路由到从库
     * @param unknown_type $ok
     */
	public static function setSlaveOk($ok=true){
        if( $ok ){
            self::$connection->setReadPreference(MongoClient::RP_SECONDARY_PREFERRED,array());
       }else{
            self::$connection->setReadPreference(MongoClient::RP_PRIMARY_PREFERRED,array());
       }
        
        return $ok;
    }


    /*
   * 获取是否对当前集合启用从库查询
   */
    public static function getSlaveOkay(){
        return self::$connection->getSlaveOkay();
    }

    //转换对象变量
    public function toValid() {
        //$p = clone $this;
        foreach (get_object_vars($this) as $k=>$v) {
            unset($this->$k);
            $this->$k = $v;
        }
    }
    //取得不完整的dao实体的属性用于update
    public function getImperfectAttributes() {
    	$attributes = [];
        $this->toValid();
    	foreach (static::$_schema as $key=>$value) {
    		if (isset($this->$key))
    			$attributes[$key] = $this->$key;
    	}
    
    	return $attributes;
    }

    //取得完整的dao实体的属性(没有的字段自动补默认值)用于insert save
    public function getAttributes() {
    	$attributes = [];
        $this->toValid();
    	foreach (static::$_schema as $key=>$value) {
    		if (!isset($this->$key))
    			$this->$key = null; 
    
    		$attributes[$key] = $this->$key;
    	}
    	 
    	return $attributes;
    }
    
    /**
     * 保存一个对象的变更到数据库中
     * 如果相同_id的对象已经存在则执行更新操作覆盖数据库的记录
     * 如果没有设置 _id，则会插入新记录，并将新插入自动生成的_id保存到当前对象上
     * @param array $options
     * @param    $options 选项详情： 是否安全插入 safe:true,false
     * 是否同步到硬盘 fsync:true,false
     * 超时时间设置timeout: If "safe" is set, this sets how long (in milliseconds) for the client to wait for a database response
     * @param insertonly     是否只执行插入操作: insertonly :  true,false  当设置为true时，如果当前数据的_id已经在数据库中，则会抛出异常 (仅当options的safe为true时才会获得异常)
     */
    public function save(array $options = array(), $insert_only = false) {
    	$this->_fmt(); //格式化一下，控制入库数据质量
    	$this->beforeSave();
    	$collection = self::getCollection();
    	$btime = time();
    	
    	if (!$this->_new && !$insert_only && $this->_imperfect)
    		return $this->update($options);
    	/*
    	if ($this->_new || $insert_only) {
    		$r = $collection->insert($this->getAttributes(), $options);
    		self::log_query('insert', null, $options, $collection, $btime, $r);
    	} else {
        	$r = $collection->save($this->getAttributes(), $options);
        	self::log_query(__METHOD__, null, $options, $collection, $btime, $r);
		}
        */
        $r = $collection->save($this->getAttributes(), $options);
        self::log_query(__METHOD__, null, $options, $collection, $btime, $r);

		$r = self::getExecInfo($r);
        if ($r===true)
        {
        	$this->afterSave();
        }

		return $r; 
    }
    
    
    /**
     * 将当前对象的变更保存到数据库中
     * @param boolean $overite_all   是否用当前对象完全替换数据库记录，
     *                               例如数据库中为: array(a=>1,b=>2),对象的值为array(c=>1)
     *                               当$overite_all=true,则保存后数据库中为 array(c=>1),a和b字段的值将丢失
     *                               当$overite_all=false,则保存后数据库中为array(a=>1,b=>2,c=>1),新增了一个c字段
     * @param boolean $upsert  如果不存在，是否插入
     * @return 如果设置了safe=true,返回了包含 status的数组，如果safe=false,只要$new_object的值不是空就返回true
     * @see http://www.php.net/manual/en/mongocollection.update.php
     * @author xwarrior 2012/3/8
     */
    public function update($options=array()){
        $this->_fmtImperfect(); //格式化一下，控制入库数据质量
    	$this->beforeUpdate();
        $optionsDefault = array('upsert' => false, 'multiple' => false,'safe' => false,'fsync' =>false, 'timeout' => static::$findTimeout);
        $options = array_merge($optionsDefault, $options);
		
        $btime = microtime();
        $collection = self::getCollection();
        $attributes = $this->getImperfectAttributes();
        unset($attributes['_id']);
        $r = $collection->update(['_id'=>$this->_id], ['$set'=>$attributes]);

        self::log_query(__METHOD__, $this->_id, $options, $collection, $btime, $attributes);
        
        $r = self::getExecInfo($r);
        
        if ($r===true)
        {
        	$this->afterSave();
        }
		
		return $r; 
    }

    /**
     *  批量插入数据，仅用在大量数据入库时，
     * * 注意 使用此方法数据与类型的正确性由调用都自己保证
     */
    public function batchInsert(array $docs) {
        return $this->getCollection()->batchInsert($docs);
    }
    
    //取得mongo执行结果信息
    private function getExecInfo($ret) {
    	if (is_array($ret)) {
    		if (isset($ret['err']) && $ret['err'] != null ){
    			return false;
    		}
    	}else{
    		if ( $ret == false){
    			return false;
    		}
    	}

        return true;
    } 
    
    
    /**
     * 把_id按递增数字存储，实现数字形式的增量
     * @param array $options  同save的选项
     *            是否安全插入 safe:true,false
     * 是否同步到硬盘 fsync:true,false
     * 超时时间设置timeout: If "safe" is set, this sets how long (in milliseconds) for the client to wait for a database response
     * @return 保存成功，返回true
     */
    public function numsave(array $options = array()) {
        $max_error_limit = 100;

        $options['safe'] = true;

        $maxiddoc	=	self::findAll(	array(),
            array( '_id' ),
            array( 'sort' => array('_id' => -1),
                   'limit' => 1)
        );
        if( $maxiddoc && count($maxiddoc) > 0 ){

            $this->_id	=	intval( $maxiddoc[0]->_id ) + 1;
        }else{
            $this->_id	=	1;
        }

        $i = 0;
        while( $i < $max_error_limit){
            try {

                $success = $this->save( $options,true );
            } catch (MongoCursorException $e) {
                if ( $e->getCode() == 11000){ //continue when exception is :key duplicate
                    if ($i >= $max_error_limit){
                        throw new Exception('递增插入主键失败，超过最大冲突尝试次数');
                    }
                    $success = false;
                    $this->_id += 1;
                }else{
                    throw $e;
                }
            }
            if( $success ){
                return true;
            }
            $i++;
        }

        return false;
    }

    /**
     * 获得当前对象的数组表示形式
     */
    public function toArray(){
        return $this->getImperfectAttributes();
    }
    
    /**
     * 获得当前对象的数组表示形式,把数组中ObjectId转为string
     */
    public function toDeepArray(){
    	$out = $this->toArray();
    	foreach($out as &$v) {
    		if ($v instanceof \MongoId)
    			$v = (string)$v;
    	}
    	
    	return $out;
    }
    

    /**
     * 从库中删除当前对象
     */
    public function destroy($options = array())
    {
        $this->beforeDestroy();

        if (!$this->_new) {
            $collection = self::getCollection();
            return $collection->remove(['_id' => $this->_id], $options);
        } else {
            return false;
        }
    }

    /**
     * 删除当前集合符合查询条件的数据
     * @see http://cn.php.net/manual/en/mongocollection.remove.php
     * @param   $criteria 要删除的查询条件
     * @param   $options 删除选项
     */
    public static function remove($criteria = null, $options = array()){
        if ($criteria == null){
            throw new Exception("criteria 未提供");
        }

        $collection = self::getCollection();
        $btime = microtime();
        $r = $collection->remove($criteria ,$options);
        
        self::log_query(__METHOD__, $criteria, $options, $collection, $btime, $r);
        
        return $r;
    }

    /**
     * 获取当前集合的名称
     */
    public function getName(){
        return self::getCollection();
    }


    /**
     * 执行一个查询，并返回所有的文档结果数组
     * @param   $query   查询条件  array( field1=>condition,field2=>array($op=>condition),field3...)
     * @param   $fields    查询字段  array( 'field1','field2',... )
     * @param   $options  查找选项 array ( sort=>array(field1=>1,field2=>-1,...),skip=>int,limit=>int )
     */
    public static function findAll($query = array(), $fields = array(), $options = array(), $hint='')
    {
        $begin_microtime = microtime();
        if ( null === $query ){
            $query == array();
        }
        if ( null === $fields){
            $fields = array();
        }
        if ( null === $options  ){
            $options = array();
        }

        $query = self::merge_in($query);

        $collection = self::getCollection();
        if( NULL !=$fields && count( $fields ) > 0){
            $documents = $collection->find($query,$fields);
        }else{
            $documents = $collection->find($query);
        }
        if ($hint) {
            $documents->hint($hint);
        }
        
        if (isset($options['sort'])) {
            foreach ($options['sort'] as $k => $v) {
                $options['sort'][$k] = (int)$v;
            }
            $documents->sort( $options['sort'] );
        }

        if (isset($options['skip']))
            $documents->skip($options['skip']);

        if (!isset($options['limit']) || $options['limit']===null)
        	 $options['limit'] = 100; //这里防止没写limit的现象
           
        $documents->limit($options['limit']);

        if (isset($options['asArray']) && $options['asArray'] == 1)
            $flag_as_array = true;
        else
        	$flag_as_array = false;

        $ret = array();
        
        $className = get_called_class();
        $documents->timeout($className::$findTimeout);

        //mongodb fetrue :when set batchsize,cusor will close after read batchsize rows
        if (isset($options['limit'])) {
            $documents->batchSize($options['limit']);  //optimize for read performence
        }

        while ( ($document = $documents->getNext()) != null)
        {
            if($flag_as_array)
                $ret[] = $document;
            else
                $ret[] = self::instantiate($document,false);
        }
        //add performence log xwarrior  2012/8/8
        self::log_query(__METHOD__, $query, $options, $collection, $begin_microtime, $ret);
        return $ret;
    }

    /**
     * 合并in查询中的重复条件
     * xwarrior 2012/8/8
     * @param unknown_type $query
     */
    private static function merge_in($query){
        foreach($query as $key => &$value){
            if ( is_array($value) && isset( $value['$in'] ) ){
                $value['$in'] = array_values(array_unique(  $value['$in'] ));
            }
        }
        return $query;
    }


    /**
     * 给collection中的某个或某此指定的定段增加或减少指定的数值，默为为字段+1
     * @param   $query     查询条件  array( field1=>condition,field2=>array($op=>condition),field3...)
     * @param   $fields    需要增加数值的字段  string OR array( 'field1','field2',... )
     *
     * @return  bool
     */
    public static function inc( $query	=	array(),$fields	=	array()	,$incnum	=	1,	$upsert = false,	$safe = true){
        if ( null === $query ){
            return false;
        }
        if ( null === $fields){
            return false;
        }
        $begin_microtime = microtime();
        $collection = self::getCollection();
        $options = array( 'upsert' => $upsert, 'multiple' => false,'safe' => $safe,'fsync' =>false, 'timeout' => static::$findTimeout  );

        /* inc不需要执行先查询再更新，如果不想没数据的时候插入，设置$upsert=false即可    xwarrior 2012/8/8
       if( NULL !=$fields){
           $documents = $collection->findOne($query);
       }else{
           return false;
       }
       */
       /**
        * 如果传入的数组是[a,b],会转成 [a=>$incnum,b=>$incnum]
        * 如果传入的数组是[a=>1,b=>2.5],则保持不变，实现inc不同的值
        */
        if(is_array($fields)){
            foreach($fields	as $k=>$v){
                if (is_string($k))
                    $new_fields[$k] = $v;
                else
                    $new_fields[$v]	= $incnum;
            }
        }else{
            $new_fields[$fields] = $incnum;
        }

        $addok	=	$collection->update($query,  array( '$inc' => $new_fields),$options);

        //add performence log xwarrior  2012/8/8
        self::log_query(__METHOD__,$query, array_merge($options, $new_fields), $collection, $begin_microtime,$addok);
        return $addok;

    }
    /**
     * 求collection中某字段进行分组求和	相当于mysql 的 sum
     * @param   $group_by		用来group by 的字段数组  array(id => true,name => true)
     * @param   $where			查询条件  常规的where数组  array('user_id' => '1260858')
     * $param   $sub_columns		可选，默认1时求count，不为1就只能设为integer类型的字段名称的字符串或数组,
     *                              如:  'field_name' or array('field_name_1','field_name_2')
     * @return  array			Array ( [retval] => Array ( [0] => Array ( [user_id] => 1260858 [count] => 6 ) ) [count] => 2 [keys] => 1 [ok] => 1 )
     */
    public static function sum($group_by = NULL,$where = NULL,$sub_columns=1){
        if ( $group_by == NULL && $sub_columns == 1 ){
            $ret =  self::count($where);

            return $ret;
        }

        $begin_microtime = microtime();
        $collection = self::getCollection();
        //added by xwarrior at 2012/4/21 for suport no field to group by
        if( $group_by == NULL ){
            $group_by= new MongoCode('function(doc) { return {any:1}; }');
        }

        if ( is_array($sub_columns)){  //added by xwarrior for suport multi field sum
            $sum = 'prev.count +=1 ;';
            $initial = array('count' => 0);
            foreach( $sub_columns as $field_name  ){
                $initial[ $field_name ] = 0;
                $sum .= "prev.$field_name += doc.$field_name;";
            }
            $reduce = new MongoCode("function(doc, prev) { $sum }");
        }else if ($sub_columns==1){
            $initial =  array('count' => 0,'sum' => 0);
            $reduce = new MongoCode('function(doc, prev) { prev.count +=1; }');
        }else{
            $initial =  array('count' => 0,'sum' => 0);
            $reduce = new MongoCode(  'function(doc, prev) { prev.count +=1 ; prev.sum += doc.'.$sub_columns.'; }' );
        }

        $ret =	$collection->group(	$group_by,								// fields to group by
            $initial,								// initial value of the aggregation counter object.
            $reduce,								// a function that takes two arguments (the current document and the aggregation to this point) and does the aggregation
            array('condition'=>$where)				// condition for including a document in the aggregation
        );

        //add performence log xwarrior  2012/8/8
        if(!$where){
            $where = array();
        }
        if(!$group_by){
            $group_by = array();
        }
        self::log_query(__METHOD__,$where, array(), $collection, $begin_microtime,$ret);

        return $ret;

    }



    /**
     * 执行一个查询，返回查询游标
     * @param   $query    查询条件 array( field1=>condition,field2=>array($op=>condition),field3...)
     * @param   $fields     查询字段 array( 'field1','field2',... )
     * @param   $options   查找选项 array ( sort=>array,skip=>int,limit=>int )
     */
    public static function find($query = array(), $fields = array(), $options = array())
    {
        $begin_microtime = microtime();
        $collection = self::getCollection();

        $query = self::merge_in($query);
        if( NULL !=$fields && count( $fields ) > 0){
            $documents = $collection->find($query,$fields);
        }else{
            $documents = $collection->find($query);
        }

        $className = get_called_class();

        if (isset($options['sort'])) {
            foreach ($options['sort'] as $k => $v) {
                $options['sort'][$k] = (int)$v;
            }
            $documents->sort( $options['sort'] );
        }

        if (isset($options['skip']))
            $documents->skip($options['skip']);

        if (!isset($options['limit']))
        	$options['limit'] = 100;
        	
        $documents->limit($options['limit']);

        $documents->timeout($className::$findTimeout);

        $ret = new MongoRecordIterator($documents, $className);

        //add performence log xwarrior  2012/8/8
        self::log_query(__METHOD__,$query, $options, $collection, $begin_microtime,$ret);
        return $ret;
    }

    /**
     * 查找一条单独的记录，如果有多条结果，只返回第一条
     * @param unknown_type $query   查询条件array( field1=>condition1,field2=>condition2);
     * @param unknown_type $fields　  查询字段列表 array('fild1','field2')
     * @RETURN 数据对象，未查找到返回null
     */
    public static function findOne($query = array(), $fields = array())
    {
        $begin_microtime = microtime();
        $query = self::merge_in($query);
        $collection = self::getCollection();
        if( $fields != null && count( $fields) > 0 ){
            $document = $collection->findOne($query,$fields);
        }else{
            $document = $collection->findOne($query);
        }

        $ret =  self::instantiate($document);

        //add performence log xwarrior  2012/8/8
        self::log_query(__METHOD__, $query, array(), $collection, $begin_microtime,$ret);

        return $ret;
    }

    public static function findById(\MongoId $id) {
        return self::findOne(['_id'=>$id]);
    }

    /**
     * 查找指定id的数据
     * @param string or objectid or array $arr_ids  要查找的id数组(string or MongoId) 如: array('4f92a1768749160f74000001' ,  '4f92a1768749160f74000001' ,  '4f92a1768749160f74000001')
     * @param array $other_query_condtion   ids之外的其它查询条件  如 : array( 'visibility' => 0  , owner_id => '2233334' )
     * @param $conver_to_key_value 是否转换为array( _id=>array() ,_id=array())的关联数组
     * @param   $fields    查询字段  array( 'field1','field2',... )
     * @param   $options  查找选项 array ( sort=>array(field1=>1,field2=>-1,...),skip=>int,limit=>int )
     * @return 返回符合条件文档的数组结果
     */
    public static function findByIds($arr_ids = array(),$other_query_condtion = array() , $conver_to_key_value = false,
                                     $fields = array(), $options = array()){
        $begin_microtime = microtime();
        if ( count($arr_ids) == 0 ){
            return array();
        }
        $or_ids = array();

        foreach($arr_ids as $id){

            if ( $id instanceof MongoId  ){
                $_id = $id;
            }else{
                $_id = new MongoId( strval($id) );  //try convert to mongoid
                if (  strval($_id) !=  $id){
                    $_id = $id;
                }
            }
            $or_ids[] =  $_id;
        }
        $query = array( '_id' => array( '$in' => $or_ids ) );
        if ( $other_query_condtion ){
            foreach($other_query_condtion as $key => $value ){
                $query[$key] = $value;
            }
        }
        $ret = self::findAll($query,$fields,$options);
        //转换为key,value关联数组
        if ( $ret && $conver_to_key_value ){
            $newret = array();
            foreach($ret as $item){
                $newret[ $item->getId() ] = $item;
            }
            $ret =  $newret;
        }
        
        self::log_query(__METHOD__, $query, array(), $collection, $begin_microtime,$ret);
        return $ret;
    }


    /**
     * 获取指定查询的记录总数
     * @param unknown_type $query
     */
    public static function count($query = array())
    {
        $begin_microtime = microtime();
        $collection = self::getCollection();
        $documents = $collection->count($query);
        $ret =  $documents;

        //记录sql执行时间  xwarrior 2012/8/10
        self::log_query(__METHOD__, $query, array(), $collection, $begin_microtime,$ret);
        return $ret;
    }


    private static function instantiate($document)
    {
        if ($document)
        {
            $className = get_called_class();
            return new $className($document, false);
        }
        else
        {
            return null;
        }
    }


    public function __toArray() {
        return $this->toArray();
    }


    private static function _castBaseType($type, $value) {
        switch ($type) {
            case 'bool':
                $v = (bool)$value;
                break;
            case 'int':
                $v = intval($value);
                break;
            case 'float':
                $v = floatval($value);
                break;
            case 'string':
                $v = strval($value);
                break;
            default:
                $v = $value;
        }
        
        return $v;
    }

    //不是数据库里字段的用"_"开头才能赋值，不会保存在库里
    private function _set($key, $value) {
        if (!isset(static::$_schema[$key])) {
            if ($key[0]=='_')
                return $this->$key = $value;
            else
                return false;
        }
        $schema = static::$_schema[$key];
        if ($value===null)
        	$value = $schema[1];
        
        $schemaExt = isset($schema[2])? $schema[2]:null;

        switch ($schema[0]) {
            case 'bool':
            case 'int':
            case 'float':
            case 'string':
                $this->$key = self::_castBaseType($schema[0], $value);
                break;
            case 'object_id':
                $this->$key = ($value instanceof \MongoId)? $value:(new \MongoId($value));
                break;
            case 'list':
            	if (empty($value))
            		$value = [];
            		
                if (isset($schema[2])) {
                    foreach ($value as $k => $v) {
                        $value[$k] = self::_castBaseType($schema[2], $v);
                    }
                }
                if (is_array($value))
                    $this->$key = array_values($value);
                break;
            case 'dict':
                if (empty($value)) {
                    $value = new \stdClass();
                } else {
                    if (!is_array($value) && !is_object($value))
                        $value = (array)$value;
                }
                $this->$key = $value;
                break;
            case 'timestamp': //default now
            	$this->$key = $value===null? time():$value;
            	break;
            case 'iso_date': //default now                
                if ($value) {
                    if ($value instanceof \MongoDate) {
                        $this->$key = $value;
                    } else if (is_string($value)) {
                        $this->$key = new \MongoDate(strtotime($value));
                    } else {
                        $this->$key = new \MongoDate($value);
                    }
                } else {
                    $this->$key = new \MongoDate();
                }       
                break;
            case 'auto_increment':
                if ($value) {
                    $this->$key = $value;
                    break;
                }
                //保存自增长id的表, 得保证有seq表
                $option = array(
                    'init' => 1,
                    'step' => 1,
                );
				$class =explode('\\', get_class($this));
                $namespace = $schemaExt? $schemaExt:lcfirst(array_pop($class));
                //$namespace = $schemaExt? $schemaExt:lcfirst(array_pop(explode('\\', get_class($this))));
                $seq = self::$connection->selectDB(self::$database)->command(array(
                    'findAndModify' => 'seq',
                    'query'         => array('_id' => $namespace),
                    'update'        => array('$inc' => array('id' => $option['step'])),
                    'new'           => true,
                	'upsert'        => true
                ));

                if (isset($seq['value']['id'])) {
                    $this->$key = $seq['value']['id'];
                } else {
                    new \Exception("$namespace:: $key自增出错");
                }
                break;
            
            default:
                $this->$key = $value;
                break;
        }
    }

    // framework overrides/callbacks:
    public function beforeUpdate() {}
    public function beforeSave() {}
    public function afterSave() {}
    public function beforeValidation() {}
    public function afterValidation() {}
    public function beforeDestroy() {}
    public function afterNew() {}
   
    // core conventions
    protected static function getCollection() {
        $className = get_called_class();
        
        if (null !== static::$collectionName) {
            $collectionName = static::$collectionName;
        } else {
            $tmp = explode('\\',$className);
            $real_className = array_pop($tmp);
            $collectionName = lcfirst($real_className);
        }

        if ($className::$database == null)
            throw new \Exception("BaseMongoRecord::database must be initialized to a proper database string");

        if ($className::$connection == null)
            throw new \Exception("BaseMongoRecord::connection must be initialized to a valid Mongo object");

        if (!($className::$connection->getConnections()))
            $className::$connection->connect();

        return $className::$connection->selectCollection($className::$database, $collectionName);
    }

    public static function setFindTimeout($timeout) {
        $className = get_called_class();
        $className::$findTimeout = $timeout;
    }

    /**
     * 建立当前集合上的索引，或确保索引建立,如果已经有索引则忽略
     * 警告：在繁忙的生产系统上对大集合数据调用ensureIndex会导致系统阴塞
     * @param array $keys
     * @param array $options
     */
    public static function ensureIndex(array $keys, array $options = array()) {
        return self::getCollection()->ensureIndex($keys, $options);
    }

    /**
     * 删除集合上的索引
     * @param unknown_type $keys
     */
    public static function deleteIndex($keys) {
        return self::getCollection()->deleteIndex($keys);
    }

    

    /**
     * 往对象的array类型字段中追加一条数据
     * @param $field_values   array( filed => values, field => value ...)
     *
     */
    public function Push($field_values,$safe = false){
        $begin_microtime = microtime();
        $options = array( 'upsert' => false, 'multiple' => false,'safe' => $safe,'fsync' =>false, 'timeout' => static::$findTimeout  );

        $critera = array('_id' => $this->attributes['_id'] );
        $ret = null;
        $collection = $this->getCollection();

        $ret = $collection->update($critera,  array('$push' => $field_values), $options);

        self::log_query('Push',$critera, $options, $collection, $begin_microtime,$ret);
        return $ret;
    }

    /**
     * 在内嵌文档中删除指定条件的数据
     * $remove_query  要移除的内嵌文档的数据条件 如 array( comment_id: ’xxxxxxxxxxxxxxxx' );
     */
    public function Pull($remove_query,$safe = false){
        $begin_microtime = microtime();
        $options = array( 'upsert' => false, 'multiple' => false,'safe' => $safe,'fsync' =>false, 'timeout' => static::$findTimeout  );

        $critera = array('_id' => $this->attributes['_id'] );
        $ret = null;
        $collection = $this->getCollection();

        $ret = $collection->update($critera,  array('$pull' => $remove_query), $options);

        $query = array_merge($critera,$remove_query);
        self::log_query(__METHOD__, $query, $options, $collection, $begin_microtime,$ret);
        return $ret;
    }


    /**
     * 批量更新符合query条件的一批数据，用fields中指定的字段值
     * @param array   $query  mongo  查询字段 		   array( filed1=>condition1,field2=>condition2,...)
     * @param object $new_object  要更新的对象值必须继承自BaseMongoRecord (为了数据验证需要)
     * @param  array  $options array("upsert" => <boolean>,"multiple" => <boolean>,"safe" => <boolean|int>,"fsync" => <boolean>, "timeout" => <milliseconds>)
     * // @return 如果设置了safe=true,返回了包含 status的数组，如果safe=false,只要$new_object的值不是空就返回true
     * @return 默认返回更新条数 //by jimmy
     *  @author  xwarrior at 2012/3/8
     * @see http://www.php.net/manual/en/mongocollection.update.php
     */
    public static function updateAll($query = array() , $attrs,
                                     $options = array( 'upsert' => false, 'multiple' => true,'safe' => true,'fsync' =>false, 'timeout' => 20000  )){
        if (!is_array($attrs)) {
            $attrs = (array)$attrs;            
        }
        unset($attrs['_id']);  //防止传入_id
        $collection = self::getCollection();
        $collectionName = $collection->getName();
        
        if (empty($attrs) ){
            throw new Exception($collectionName. '要保存的对象值不能为空');
        }

        if (empty($query)){
            throw new Exception($collectionName.'更新的查询条件不能为空');
        }

        $ret =  self::getCollection()->update($query,array('$set'=>$attrs),$options);

        if (!empty($options['safe'])) 
            return $ret['n'];
        else 
            return $ret;

    }

    public static function distinct($key, array $query) {
        return self::getCollection()->distinct($key, $query);
    }
    
    /**
     * 
     */
    public function getId() {
        return strval($this->_id);
    }

    /**
     * 记录查询日志
     * xwarrior @ 2012/8/8
     * @param unknown_type $query
     * @param unknown_type $options
     * @param unknown_type $collection
     * @param unknown_type $begin_microtime
     */
    private static function log_query($method,$query,$options,$collection,$begin_microtime,$ret){
        if (empty($_SERVER['fb_debug']))
        return '';
    	$stacktrace = '';
    
    	/* TODO: 根据日志级别决定是否显示调用堆栈 */
    	$stack_list = debug_backtrace() ;
    	$stacks = array();
    	foreach($stack_list as $stack){
    		@$stacks[] = 'at file:'. $stack['file']  . ' line:' . $stack['line'] . ' function:' . $stack['function'];
    	}
    	$stacktrace = implode('\n',$stacks);
    	$colname = '';
    	if( $collection ){
    		$colname = $collection->getName();
    	}
    	
    	$table['server'] = BaseMongoRecord::$connection.'/'.BaseMongoRecord::$database;
    	$table['collection'] = $colname;
    	$table['query'] = 'invoke:' . $method . ' query:' . json_encode( $query ) . '  options:'.  json_encode($options)  . "\n$stacktrace";;
    	$table['time'] = microtime() - $begin_microtime;
    	$table['result'] = 'len: '.count($ret);
        if (isset($_GET['_db_debug'])) {
            echo '<!--';
            print_r($table);
            echo '-->';
        }
    	Debug::table('mongo', $table);
    
    }


    /**
     * 检查一个给定的mongoid是否是有效的格式
     * @author xwarrior at 2012.5.17
     * @param unknown_type $mongoid
     */
    public static function isMongoIDValid($mongoid){
        if ( !$mongoid ){
            return false;
        }

        if ( $mongoid instanceof MongoId  ){
            return true;
        }

        $oid = new MongoId( strval($mongoid) );  //try convert to mongoid
        if (  strval($oid) !=  $mongoid){
            return false;
        }
        return True;
    }

    /**
     * 读取当前时间的整数millsecond表示形式
     * yoka at项目建议统一使用该以毫秒ms为单位的时间
     * @see 只支持64位系统下的php
     */
    public static function Millseconds() {
        list($mills,$seconds )  =   explode( ' ',microtime()) ;
        $microtime =  $seconds .  substr(  $mills ,2,3);

        return intval($microtime);
    }

    /**
     * 读取当前时间,整数millsecond表示形式
     * yoka at项目建议统一使用该以毫秒ms为单位的时间
     * @see 只支持64位系统下的php
     */
    public static function now(){
        return self::Millseconds();
    }

    /**
     * 20120718 by sam add
     * 字符串或字符串id数组,转换到,mongoId或mongoId数组
     * @param $ids 值格式：'id' 或 'id1,id2,idN' 或  array(id1,id2,id3,idN)
     * @return new MongoId() 或  array(new MongoId,new MongoId,....);
     */
    public static function toMongoId($ids){
        if(!is_array($ids)){
            if(strstr($ids,',')){
                $ids=explode(",",$ids);
            }else{
                return new MongoId($ids);
            }
        }
        $c=count($ids);
        if($c<1)
            return null;
        else if($ids[$c-1]==''){
            array_pop($ids);
        }
        foreach ($ids as &$id){
            $id=new MongoId(trim($id));
        }
        return $ids;
    }
    
    //---------------- @since 2012.11.21 by liweiwei ---------------
    /**
     * 按id顺序返回查找结果
     * @param int $ids 商品id
     * @return 按传入id顺序返回
     */
    public static function getByIds($ids) {
    	$out = array();
    	foreach ($ids as $v) {
    		$out[(string)$v] = null;
    	}
    
    	$items = self::findByIds($ids);
    	foreach ($items as $v) {
    		$out[(string)$v->$key] = $v;
    	}
    
    	$out = array_filter($out);
    
    	return $out;
    }

    public static function getSchema() {
        return static::$_schema;
    }

    public function jsonSerialize() {
        $p = $this;
        foreach (static::$_schema as $key => $value) {
            if ($value[0]=='iso_date') {
                if (isset($p->$key) && isset($p->$key->sec))
                    //$p->$key = date('c', $p->$key->sec);
                    $p->$key = date('Y-m-d\TG:i:s\Z', $p->$key->sec);
            }
        }

        return $p;
    }
    
    
    
    //连接mongodb xwarrior @2012/7/20
    function connect_mongodb()
    {
    	$CfgName=$this->getMongoCfgName();
    	$mongodbinfo=\SpriteConfig::getMongoDbs($CfgName);
    	if(empty($mongodbinfo)){
			die('Mongo配置信息错误');    		
    	}
    	$mongo_host=$mongodbinfo['host'];
    	$mongo_db==$mongodbinfo['dbname'];
    	MongoLog::setLevel(MongoLog::WARNING); // all log levels
    	//设置连接池大小（与php进程数一致) DEPRECATED in driver: 1.2.11
    	//Mongo::setPoolSize(256);
    	BaseMongoRecord::$connection = new MongoClient($mongo_host);
    	BaseMongoRecord::$database =   $mongo_db;
    	//启用从库读
    	BaseMongoRecord::setSlaveOk(true);
    
    	return true;
    }
    
}

// ----------------- 初始化数据库连接 -----------------
ini_set('mongo.native_long', 1);  //启用64位整数
