<?php
namespace sprite\lib;
use \SpriteConfig;

/**
 * memcached缓存
 * 
 * @author 23003
 *
 */
abstract class MCache {

	private static $_memcached;
	/**
	 * 获取需要调用的配置的名字
	 */
	protected abstract static function getCfgName();
	/**
	 * 配置文件里配置
	 * $servers = [['127.0.0.1', '11211'],['127.0.0.1','11211']];
	 * @param unknown $servers 服务器列表
	 * @return \Memcached
	 */
	public static function getInstance()
	{
		if (empty(self::$_memcached)) {
			self::$_memcached = new \Memcached();
			$servers=\SpriteConfig::getMemcacheServers(self::getCfgName());
			self::$_memcached->addServers($servers);
		}
		return self::$_memcached;
	}
}