<?php
namespace sprite\lib;

/**
 * redis 缓存
 *
 * @author 23003
 *
 */
abstract class RCache {
	
	private static $_redis;
	/**
	 * 获取需要调用的配置的名字
	 */
	protected abstract static function getCfgName();
	/**
	 * 配置文件里配置
	 * @param unknown $servers 服务器信息
	 * @return \Memcached
	 */
	public static function getInstance()
	{
		if (empty(self::$_redis)) {
			self::$_redis = new \Redis();
			$servers=\SpriteConfig::Getredisserver(self::getCfgName());
			$host=$servers['host'];
			$port=$servers['port'];
			$pwd=$servers['password'];
			if(!empty($pwd))
			{
				self::$_redis->auth($pwd);
			}
			self::$_redis->connect($host, $port);
		}
		return self::$_redis;
	}
}