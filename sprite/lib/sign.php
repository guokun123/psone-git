<?php
namespace app\Junior;

/**
 * 简单的签名方法
 * @author li.weiwei@163.com
 *
 */
class Sign {
    
    public static function getSign($key, array $data) {
        ksort($data);
        $str = implode($key, $data);
        
        return substr(md5($str), 1, -1);
    }
    
}
