<?php
namespace sprite\logger;

/**
 * 分级日志记录
 * @author li.weiwei@163.com
 *
 */
class Logger {
	
	const NOSET = 0;
	const INFO = 10;
	const DEBUG = 20;
	const WARNING = 30;
	const ERROR = 40;
	const CRITICAL = 50;
	
	private $_fileName;
	private $_format;
	private $_level;
	private $_loggerStream;
	private $_datfmt;
	private $_loggerName;
	private $_configSetting = false;
	
    private static $levels = [
        self::NOSET    => 'NOSET', 
        self::INFO     => 'INFO',
        self::DEBUG    => 'DEBUG',
        self::WARNING  => 'WARN', 
        self::ERROR    => 'ERROR',
        self::CRITICAL => 'CRITI']; //记录日志时对齐
	private static $logger = [];

	//@todo stream handle
	public function __construct($loggerName, $fileName, $datefmt='Y-m-d H:i:s', $level= self::WARNING) {
		$this->_fileName = $fileName;
		$this->_level = $level;
		$this->_datefmt = $datefmt;
		$this->_loggerName = $loggerName;
		$path = dirname($this->_fileName);
		if (!file_exists($path))
			mkdir($path, 0700);

		self::$logger[$this->_loggerName] = $this;
	}

	public static function getLogger($loggerName='main') {
		if (empty(self::$logger[$loggerName]))
            throw new \Exception('no logger named '.$loggerName);
			
		return self::$logger[$loggerName];
	}
	
	
	public function setLevel($level) {
		$this->_level=  $level;
	}
	
	public function log($level, $msg) {
		if ($level<$this->_level)
			return true;
		
		$levelName = self::$levels[$this->_level];
		$msgs = sprintf("%s %s %-5s %s\n", date($this->_datefmt), $this->_loggerName, self::$levels[$this->_level], $msg);
		
		error_log($msgs, 3, $this->_fileName);
	}

	public function info($msg) {
		return $this->log(self::INFO, $msg);
	}
	
	public function debug($msg) {
		return $this->log(self::DEBUG, $msg);
	}

	public function warning($msg) {
		return $this->log(self::WARNING, $msg);
	}
	
	public function error($msg) {
		return $this->log(self::ERROR, $msg);
	}
	
	public function critical($msg) {
		return $this->log(self::CRITICAL, $msg);
	}

}
